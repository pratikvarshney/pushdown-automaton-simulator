# README #

* Push Down Automata simulator
* Output: check if input string is accepted by given pda or not

### INPUT ###
* Q : a finite set of states.
* q0: the initial state from where any input is processed (belongs to Q)
* F : a set of final state/states of Q (subset of Q)
* E : a finite set of symbols called the alphabet.
* T : finite set which is called the stack alphabet.
* Z0: the initial stack symbol (belongs to T)
* & : the transition function where &: Q x (E U {e}) x T -> Q x T*

### Example : accept pallindrome by empty stack condition ###
```
#!html

Q  : q1,q2
q0 : q1
F  : !
E  : 0,1
T  : R,B,G
Z0 : R

DELTA :
d(q1,0,R)       =       {(q1,B.R)}
d(q1,1,R)       =       {(q1,G.R)}
d(q1,0,B)       =       {(q1,B.B),(q2,@)}
d(q1,0,G)       =       {(q1,B.G)}
d(q1,1,B)       =       {(q1,G.B)}
d(q1,1,G)       =       {(q1,G.G),(q2,@)}
d(q2,0,B)       =       {(q2,@)}
d(q2,1,G)       =       {(q2,@)}
d(q1,@,R)       =       {(q2,@)}
d(q2,@,R)       =       {(q2,@)}

Enter your input string: 1001

--------------------------------------------------------------------------------
Processing ID : (q1, 1001, R)
   -> Adding ID to FIFO-Queue : (q1, 001, G.R)
   -@ Adding ID to FIFO-Queue : (q2, 1001, @)

--------------------------------------------------------------------------------
Processing ID : (q1, 001, G.R)
   -> Adding ID to FIFO-Queue : (q1, 01, B.G.R)

--------------------------------------------------------------------------------
Processing ID : (q2, 1001, @)

--------------------------------------------------------------------------------
Processing ID : (q1, 01, B.G.R)
   -> Adding ID to FIFO-Queue : (q1, 1, B.B.G.R)
   -> Adding ID to FIFO-Queue : (q2, 1, G.R)

--------------------------------------------------------------------------------
Processing ID : (q1, 1, B.B.G.R)
   -> Adding ID to FIFO-Queue : (q1, @, G.B.B.G.R)

--------------------------------------------------------------------------------
Processing ID : (q2, 1, G.R)
   -> Adding ID to FIFO-Queue : (q2, @, R)

--------------------------------------------------------------------------------
Processing ID : (q1, @, G.B.B.G.R)

--------------------------------------------------------------------------------
Processing ID : (q2, @, R)

ID : (q2, @, @)

                    ----------------------------------------
                    |  RESULT : Acceptance by Stack Empty  |
                    ----------------------------------------

================================================================================

```