import java.io.*;
import java.util.*;

//Q,q0,F,E,T,Z0,&
class PDA
{
    static String epsilon="@";
    static String phi="!";
    static String[] Q;
    static String q0;
    static String[] F;
    static String[] E;
    static String Z0;
    static ArrayList delta;
    static Queue que = new LinkedList();
    public static void main(String[] args)
    {
        try{
            Scanner sc = new Scanner(System.in);
            BufferedReader br;
            String str;
            if(args.length==0) 
            {
                System.out.print("Enter Input file path: ");
                str=sc.next();
            }
            else str=args[0];
            br = new BufferedReader(new InputStreamReader(new FileInputStream(str)));
            
            //Read Q
            str = br.readLine();
            Q=str.split(",");

            //Read q0
            q0 = br.readLine();

            //Read F
            str = br.readLine();
            F=str.split(",");
            
            //Read E
            str = br.readLine();
            E=str.split(",");
            
            //Read T
            str = br.readLine();
            String[] T;
            T=str.split(",");
            
            //Read Z0
            Z0 = br.readLine();
            
            //Read &
            delta = new ArrayList();
            while(true)
            {
                str = br.readLine();
                if(str==null) break;
                
                delta.add(new Moves(str));
            }
            
            //DISPLAY
            System.out.print("Q  : ");
            if(Q.length>0) System.out.print(Q[0]);
            for(int i=1; i<Q.length; i++)
            {
                System.out.print(","+Q[i]);
            }
            System.out.println();
            
            System.out.println("q0 : "+q0);
            
            System.out.print("F  : ");
            if(F.length>0) System.out.print(F[0]);
            for(int i=1; i<F.length; i++)
            {
                System.out.print(","+F[i]);
            }
            System.out.println();
            
            System.out.print("E  : ");
            if(E.length>0) System.out.print(E[0]);
            for(int i=1; i<E.length; i++)
            {
                System.out.print(","+E[i]);
            }
            System.out.println();
            
            System.out.print("T  : ");
            if(T.length>0) System.out.print(T[0]);
            for(int i=1; i<T.length; i++)
            {
                System.out.print(","+T[i]);
            }
            System.out.println();
            
            System.out.println("Z0 : "+Z0);
            System.out.println();
            
            System.out.println("DELTA :");
            Moves m;
            py p_gamma;
            for(int i=0; i<delta.size(); i++)
            {
                m=(Moves)delta.get(i);
                System.out.print("d("+m.q+","+m.a+","+m.Z+")\t=\t{");
                for(int j=0; j<m.val.size(); j++)
                {
                    p_gamma=(py)m.val.get(j);
                    if(j>0) System.out.print(",");
                    System.out.print("("+p_gamma.p+","+p_gamma.y[0]);
                    for(int k=1; k<p_gamma.y.length; k++)
                    {
                        System.out.print("."+p_gamma.y[k]);
                    }
                    System.out.print(")");
                }
                System.out.print("}");
                System.out.println();
            }
            
            while(true)
            {
                System.out.print("\nEnter your input string: ");
                String input = sc.next();
                Stack s=new Stack();
                s.push(Z0);
                que.clear();
                que.add(new InstantDesc(q0, input, s));
                boolean accept=false;
                InstantDesc ID;
                while(!que.isEmpty())
                {
                    ID=(InstantDesc)que.poll();
                    System.out.print("\n--------------------------------------------------------------------------------");
                    System.out.print("\nProcessing ID : ");
                    showID(ID);
                    accept = transition(ID.state, ID.w, ID.Z);
                    if(accept) break;
                }
                if(!accept) System.out.println("\n\n\t\t\t ----------------------------\n\t\t\t | RESULT : Not Accepted!!! |\n\t\t\t ----------------------------\n");
                System.out.print("\n================================================================================");
                System.out.print("\nPress y to enter a new input, OR any other key to exit: ");
                if(!sc.next().equalsIgnoreCase("y")) break;
            }
            
        }catch(Exception e)
        {
            System.out.println(e);
        }
    }
    public static void showID(InstantDesc ID)
    {
        System.out.print("("+ID.state+", "+((ID.w.length()==0)?epsilon:ID.w)+", ");
        if(ID.Z.isEmpty())System.out.print(epsilon);
        ListIterator<String> itr=ID.Z.listIterator(ID.Z.size());
        boolean first=true;
        while(itr.hasPrevious())
        {
            if(!first){
                System.out.print(".");
            }
            System.out.print((String)itr.previous());
            first=false;
        }
        System.out.println(")");
        return;
    }
    public static Moves getMoves(String q, String a, String Z)
    {
        Moves m;
        for(int i=0; i<delta.size(); i++)
        {
            m=(Moves)delta.get(i);
            if(m.q.equals(q) && m.a.equals(a) && m.Z.equals(Z)) return m;
        }
        return null;
    }
    
    public static boolean transition(String q, String w, Stack Z)
    {
        Moves m=null;
        InstantDesc ID;
        if(Z.empty()){
            return false;
        }
        if(w.length()!=0 && !w.equals(epsilon)) m = getMoves(q, w.substring(0,1), (String)Z.peek());
        Moves me = getMoves(q, epsilon , (String)Z.peek());
        Z.pop();
        
        py p_gamma;
        Stack n;
        if(m!=null)
            for(int i=0; i<m.val.size(); i++)
            {
                p_gamma=(py)m.val.get(i);
                n=(Stack)Z.clone();
                for(int j=p_gamma.y.length-1; j>=0; j--)
                {
                    if(!p_gamma.y[j].equals(epsilon)) n.push(p_gamma.y[j]);
                }
                //condition
                ID=new InstantDesc(p_gamma.p, w.substring(1), n);
                if(isAccepted(ID)) return true;
                else
                {
                    System.out.print("   -> Adding ID to FIFO-Queue : ");
                    showID(ID);
                    que.add(ID);
                }
            }
        
        //for Epsilon
        if(me!=null)
            for(int i=0; i<me.val.size(); i++)
            {
                p_gamma=(py)me.val.get(i);
                n=(Stack)Z.clone();
                for(int j=p_gamma.y.length-1; j>=0; j--)
                {
                    if(!p_gamma.y[j].equals(epsilon)) n.push(p_gamma.y[j]);
                }
                //condition
                ID=new InstantDesc(p_gamma.p, w, n);
                if(isAccepted(ID)) return true;
                else
                {
                    System.out.print("   -@ Adding ID to FIFO-Queue : ");
                    showID(ID);
                    que.add(ID);
                }
            }
        return false;
    }
    public static boolean isAccepted(InstantDesc ID)
    {
        if(ID.w.length()==0 && ID.Z.empty() && isFinal(ID.state) ){
            System.out.print("\nID : ");
            showID(ID);
            System.out.println();
            System.out.println("\t     ------------------------------------------------------");
            System.out.println("\t     |  RESULT : Acceptance by Stack Empty / Final State  |");
            System.out.println("\t     ------------------------------------------------------");
            return true;
        }
		if(isFinal(ID.state) && ID.w.length()==0){
            System.out.print("\nID : ");
            showID(ID);
            System.out.println();
            System.out.println("\t\t    ----------------------------------------");
            System.out.println("\t\t    |  RESULT : Acceptance by Final State  |");
            System.out.println("\t\t    ----------------------------------------");
            return true;
        }
        if(ID.w.length()==0 && ID.Z.empty()){
            System.out.print("\nID : ");
            showID(ID);
            System.out.println();
            System.out.println("\t\t    ----------------------------------------");
            System.out.println("\t\t    |  RESULT : Acceptance by Stack Empty  |");
            System.out.println("\t\t    ----------------------------------------");
            return true;
        }
        return false;
    }
    public static boolean isFinal(String f)
    {
        if(F[0].equals(phi)) return false;
        for(int i=0; i<F.length; i++)
        {
            if(F[i].equals(f)) return true;
        }
        return false;
    }
}
class Moves
{
    String q,a,Z;
    ArrayList val;
    public Moves(String str)
    {
        StringTokenizer st = new StringTokenizer(str,",");
        q=st.nextToken();
        a=st.nextToken();
        Z=st.nextToken();
        val=new ArrayList();
        String p;
        String y;
        while(st.hasMoreTokens())
        {
            p=st.nextToken();
            y=st.nextToken();
            val.add(new py(p,y));
        }
    }
}
class py
{
    String p;
    String[] y;
    public py(String state, String gamma)
    {
        p=state;
        StringTokenizer st = new StringTokenizer(gamma,".");
        y=new String[st.countTokens()];
        for(int i=0; st.hasMoreTokens(); i++)
        {
            y[i]=st.nextToken();
        }
    }
}
class InstantDesc
{
    String state;
    String w;
    Stack Z;
    public InstantDesc(String s, String input, Stack st)
    {
        state=s;
        w=input;
        Z=st;
    }
}